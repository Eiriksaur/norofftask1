//DOM elements
const elWorkBtn = document.getElementById('workButton');
const elDepositBtn = document.getElementById('depositButton');
const elLoanBtn = document.getElementById('loanButton');
const elComputerList = document.getElementById('dropdown');
const elSalary = document.getElementById('salary');
const elSaldo = document.getElementById('saldo');
const elNumOfCompBought = document.getElementById('computersBought');
const elCompName = document.getElementById('pcName');
const elCompPrice = document.getElementById('price');
const elCompDescription = document.getElementById('description');
const elCompImg = document.getElementById('computerImg');
const elCompfeat = document.getElementById('features')
const elBuyBtn = document.getElementById('buyButton');
const elDebt = document.getElementById('debt');
const elRepayLoanBtn = document.getElementById('repayLoanBtn');

let numOfLoans = 0;


//Object constructor for the computers
function Pc(name, price, description, image, feature){
    this.name = name;
    this.price = price;
    this.description = description;
    this.image = image;
    this.feature = feature;
}

//Creating the computer Objects and storing their names in the dropdown
let computers = [];

computers.push(new Pc("Acer chromebook 315", 2390, "En chromebook for deg som vill ha en chromebook", "https://th.bing.com/th/id/OIP.cjX-st6Jnd4h-N3HzN9augHaFW?w=243&h=180&c=7&o=5&dpr=1.5&pid=1.7", "CPU: Intell i5-5560 \n GPU: AMD RX900 \n Keyboard: Norwegian"));
computers.push(new Pc('Lenovo Thinkpad', 1600, 'Maskinen for deg som ikke liker å tenke', 'https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcT0Autj2FPnJE2uu6DavfdkWVFLnZUDdKxFghE-1u_EbrjQo_0XE1Bbw0aScGR0xmI5nNY2OCQQHQ&usqp=CAc', 'CPU: Intell i3-3700 \n GPU: Integrated* \n Keyboard: Norwegian'));
computers.push(new Pc('Asus Nitro', 9000, 'For deg som lever for gaming', 'https://encrypted-tbn3.gstatic.com/shopping?q=tbn:ANd9GcRS5AP-L0TeCWJ_yogV4lqRFOeIcWOAfi9GhJgmv4YArVtE4Y5bLyO4x5aKSpFcoy6cjXs3-5QJb2Q&usqp=CAc', 'CPU: Intell i9-8800 \n GPU: NVIDEA TI-2070 \n Keyboard: English'));
computers.push(new Pc('HP 250', 2500, 'For deg som ikke er komfortabel med å ta en 360', 'https://encrypted-tbn3.gstatic.com/shopping?q=tbn:ANd9GcSn97D_oqkvbXcu1N31LufLWyZiuGkz6qCpkqidTNPImwZ97GJebHxX3f-SvxtWEXt6jrADnGovLw&usqp=CAc', 'CPU: Intell i5-7860 \n GPU: AMD RX800 \n Keyboard: Norwegian'));
let computerDropdown = document.getElementById('dropdown');
for(i=0; i < computers.length; i++){
    let opt = document.createElement('option');
    opt.id = 'option'+i
    opt.innerText = computers[i].name;
    computerDropdown.appendChild(opt);
};


//Function that triggers on pressing the workButton
elWorkBtn.addEventListener('click', function(event){
    elSalary.innerText = parseInt(elSalary.innerText) + 100;
})

//Function that triggers on pressing the depositeButton
elDepositBtn.addEventListener('click', function(event){
    //Transfers all the funds from 'salary' to 'saldo'
    elSaldo.innerText = parseInt(elSaldo.innerText) + parseInt(elSalary.innerText);
    elSalary.innerText = 0;
});

//Function that triggers on pressing the Loan button
elLoanBtn.addEventListener('click', function(event){
    //Checking if you have bought enough computers to be allowed another loan
    if(parseInt(numOfLoans) <= parseInt(elNumOfCompBought.innerText)){
        let amount = parseInt(window.prompt('Apply for a loan'));
        //Checking that the input actually is a number
        if(Number.isInteger(amount)){
            //Checking if the amount you apply for is within you limit
            if(amount <= 2*parseInt(elSaldo.innerText)){
                window.alert('You got a loan of: ' + amount + "kr");
                elSaldo.innerText = parseInt(elSaldo.innerText) + amount;
                elDebt.innerText = parseInt(elDebt.innerText) + amount;
                numOfLoans++;
            }
            else {
                window.alert("The amount " + amount + " excedes the possible loan limit. \n You are only able to loan up to and not exceding: " + parseInt(elSaldo.innerHTML)*2 + "kr");
            }
        }
        else{
            window.alert('You have to enter a number');
        }
        
    } else {
        window.alert("You have to buy a computer before you can get a new loan.");
    }
})

//Triggers when you press the Repay Loan button
elRepayLoanBtn.addEventListener('click', function(event){
    //Repayes the entier loan given that the user can affoard it.
    let saldoAmount = parseInt(elSaldo.innerText);
    let debtAmount = parseInt(elDebt.innerText);
    if(saldoAmount < debtAmount) {
        window.alert('You dont have enough funds to pay your debt');
    } else {
        elSaldo.innerText = saldoAmount - debtAmount;
        elDebt.innerText = 0;
        window.alert('You have paid your debt');
    }

})

//Changes the computer shown when you choose an element from the dropdown
elComputerList.addEventListener('change', function(event){
    let selectedComputer = elComputerList.options[elComputerList.selectedIndex].value;
    for(i=0; i < computers.length; i++){
        if(computers[i].name == selectedComputer){
            elCompName.innerText = computers[i].name;
            elCompPrice.innerText = computers[i].price;
            elCompDescription.innerText = computers[i].description;
            elCompImg.src = computers[i].image;
            elCompfeat.innerText= computers[i].feature;
        };

    };
});

//Function that triggers when pressing the Buy Now! button.
elBuyBtn.addEventListener('click', function(event){
    //Check to make sure that customer can afford the computer
    if(parseInt(elCompPrice.innerText) <= parseInt(elSaldo.innerText)){
        elNumOfCompBought.innerText = parseInt(elNumOfCompBought.innerText) +1;
        elSaldo.innerText = parseInt(elSaldo.innerText) - parseInt(elCompPrice.innerText);
        window.alert('Congratulation on your purchase of the: ' + elCompName.innerText);
    } else {
        window.alert('Sorry you have an insufficient amount of funds in your bank');
    };
});
